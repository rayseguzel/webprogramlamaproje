﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using saumfsertifika.Models;

namespace saumfsertifika.Areas.Admin.Controllers
{
    public class KayitlisController : Controller
    {
        private sertifikaegitimEntities db = new sertifikaegitimEntities();

        // GET: Admin/Kayitlis
        public ActionResult Index()
        {
            var kayitli = db.Kayitli.Include(k => k.Admin).Include(k => k.Kursiyer);
            return View(kayitli.ToList());
        }

        // GET: Admin/Kayitlis/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kayitli kayitli = db.Kayitli.Find(id);
            if (kayitli == null)
            {
                return HttpNotFound();
            }
            return View(kayitli);
        }

        // GET: Admin/Kayitlis/Create
        public ActionResult Create()
        {
            ViewBag.ID = new SelectList(db.Admin, "ID", "ID");
            ViewBag.ID = new SelectList(db.Kursiyer, "ID", "ID");
            return View();
        }

        // POST: Admin/Kayitlis/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Ad,Soyad,Yas,Il,TelefonNo,Mail,KullaniciAdi,Sifre")] Kayitli kayitli)
        {
            if (ModelState.IsValid)
            {
                db.Kayitli.Add(kayitli);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID = new SelectList(db.Admin, "ID", "ID", kayitli.ID);
            ViewBag.ID = new SelectList(db.Kursiyer, "ID", "ID", kayitli.ID);
            return View(kayitli);
        }

        // GET: Admin/Kayitlis/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kayitli kayitli = db.Kayitli.Find(id);
            if (kayitli == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID = new SelectList(db.Admin, "ID", "ID", kayitli.ID);
            ViewBag.ID = new SelectList(db.Kursiyer, "ID", "ID", kayitli.ID);
            return View(kayitli);
        }

        // POST: Admin/Kayitlis/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Ad,Soyad,Yas,Il,TelefonNo,Mail,KullaniciAdi,Sifre")] Kayitli kayitli)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kayitli).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID = new SelectList(db.Admin, "ID", "ID", kayitli.ID);
            ViewBag.ID = new SelectList(db.Kursiyer, "ID", "ID", kayitli.ID);
            return View(kayitli);
        }

        // GET: Admin/Kayitlis/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kayitli kayitli = db.Kayitli.Find(id);
            if (kayitli == null)
            {
                return HttpNotFound();
            }
            return View(kayitli);
        }

        // POST: Admin/Kayitlis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kayitli kayitli = db.Kayitli.Find(id);
            db.Kayitli.Remove(kayitli);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
