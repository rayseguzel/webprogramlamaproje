﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using saumfsertifika.Models;

namespace saumfsertifika.Areas.Admin.Controllers
{
    public class EgitmenlersController : Controller
    { 
        private sertifikaegitimEntities db = new sertifikaegitimEntities();
        [Authorize(Users = "r.ayse.guzel@gmail.com")]
        // GET: Admin/Egitmenlers
        public ActionResult Index()
        {
            return View(db.Egitmenler.ToList());
        }

        // GET: Admin/Egitmenlers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Egitmenler egitmenler = db.Egitmenler.Find(id);
            if (egitmenler == null)
            {
                return HttpNotFound();
            }
            return View(egitmenler);
        }

        // GET: Admin/Egitmenlers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Egitmenlers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Egitmen_Resim,Egitmen_Adi,Egitmen_Unvan,Egitmen_Detay")] Egitmenler egitmenler)
        {
            if (ModelState.IsValid)
            {
                db.Egitmenler.Add(egitmenler);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(egitmenler);
        }

        // GET: Admin/Egitmenlers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Egitmenler egitmenler = db.Egitmenler.Find(id);
            if (egitmenler == null)
            {
                return HttpNotFound();
            }
            return View(egitmenler);
        }

        // POST: Admin/Egitmenlers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Egitmen_Resim,Egitmen_Adi,Egitmen_Unvan,Egitmen_Detay")] Egitmenler egitmenler)
        {
            if (ModelState.IsValid)
            {
                db.Entry(egitmenler).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(egitmenler);
        }

        // GET: Admin/Egitmenlers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Egitmenler egitmenler = db.Egitmenler.Find(id);
            if (egitmenler == null)
            {
                return HttpNotFound();
            }
            return View(egitmenler);
        }

        // POST: Admin/Egitmenlers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Egitmenler egitmenler = db.Egitmenler.Find(id);
            db.Egitmenler.Remove(egitmenler);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
