﻿using saumfsertifika.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace saumfsertifika.Controllers
{
    public class AdminController : Controller
    {
        sertifikaegitimEntities ent = new sertifikaegitimEntities();

        


        public ActionResult Slider()
        {
            var slider = ent.Slider.ToList();
            return View(slider);
        }

        public ActionResult SlideEkle()
        {
            return View();
        }


        public ActionResult SlideDuzenle(int SlideID)
        {
            var _slideDuzenle = ent.Slider.Where(x => x.ID == SlideID).FirstOrDefault();
            return View(_slideDuzenle);
        }

        public ActionResult SlideSil(int SlideID)
        {
            try
            {
                ent.Slider.Remove(ent.Slider.First(d => d.ID == SlideID));
                ent.SaveChanges();
                return RedirectToAction("Slider", "Admin");

            }
            catch (Exception ex)

            {
                throw new Exception("silerken hata oluştu", ex.InnerException);
            }

        }
    }
}