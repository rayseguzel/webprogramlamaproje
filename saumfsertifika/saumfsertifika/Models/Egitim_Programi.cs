//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace saumfsertifika.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Egitim_Programi
    {
        public int ID { get; set; }
        public string Gun { get; set; }
        public string Oturum { get; set; }
        public string Program { get; set; }
        public System.TimeSpan Sure { get; set; }
        public int FKEgitim_id { get; set; }
    
        public virtual Egitimler Egitimler { get; set; }
    }
}
