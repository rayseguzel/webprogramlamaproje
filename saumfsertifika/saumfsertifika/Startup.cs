﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(saumfsertifika.Startup))]
namespace saumfsertifika
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
