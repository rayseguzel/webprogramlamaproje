﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using saumfsertifika.Models;

namespace saumfsertifika.Areas.Admin.Controllers
{
    public class EtkinliklersController : Controller
    {
        private sertifikaegitimEntities db = new sertifikaegitimEntities();
        [Authorize(Users = "r.ayse.guzel@gmail.com")]
        // GET: Admin/Etkinliklers
        public ActionResult Index()
        {
            return View(db.Etkinlikler.ToList());
        }

        // GET: Admin/Etkinliklers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Etkinlikler etkinlikler = db.Etkinlikler.Find(id);
            if (etkinlikler == null)
            {
                return HttpNotFound();
            }
            return View(etkinlikler);
        }

        // GET: Admin/Etkinliklers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Etkinliklers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Etkinlik_Baslik,Etkinlik,Etkinlik_Tarih,Tarih")] Etkinlikler etkinlikler)
        {
            if (ModelState.IsValid)
            {
                db.Etkinlikler.Add(etkinlikler);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(etkinlikler);
        }

        // GET: Admin/Etkinliklers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Etkinlikler etkinlikler = db.Etkinlikler.Find(id);
            if (etkinlikler == null)
            {
                return HttpNotFound();
            }
            return View(etkinlikler);
        }

        // POST: Admin/Etkinliklers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Etkinlik_Baslik,Etkinlik,Etkinlik_Tarih,Tarih")] Etkinlikler etkinlikler)
        {
            if (ModelState.IsValid)
            {
                db.Entry(etkinlikler).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(etkinlikler);
        }

        // GET: Admin/Etkinliklers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Etkinlikler etkinlikler = db.Etkinlikler.Find(id);
            if (etkinlikler == null)
            {
                return HttpNotFound();
            }
            return View(etkinlikler);
        }

        // POST: Admin/Etkinliklers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Etkinlikler etkinlikler = db.Etkinlikler.Find(id);
            db.Etkinlikler.Remove(etkinlikler);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
