﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace saumfsertifika.Areas.Admin.Controllers
{
    public class AdminHomeController : Controller
    {
        [Authorize(Users ="r.ayse.guzel@gmail.com")]
        // GET: Admin/AdminHome
        public ActionResult Index()
        {
            return View();
        }
    }
}