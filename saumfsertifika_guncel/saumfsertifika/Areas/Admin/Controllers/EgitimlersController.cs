﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using saumfsertifika.Models;

namespace saumfsertifika.Areas.Admin.Controllers
{
    public class EgitimlersController : Controller
    {
        private sertifikaegitimEntities db = new sertifikaegitimEntities();

        // GET: Admin/Egitimlers
        public ActionResult Index()
        {
            return View(db.Egitimler.ToList());
        }

        // GET: Admin/Egitimlers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Egitimler egitimler = db.Egitimler.Find(id);
            if (egitimler == null)
            {
                return HttpNotFound();
            }
            return View(egitimler);
        }

        // GET: Admin/Egitimlers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Egitimlers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Egitim_Adi,Egitim_Tanitimi,Egitim_Amaci,Egitim_Faydasi,Sertifika,Yer,Zaman,Ucret,Kontenjan,Tatbikat_Bilg,Seminer_Bilg,Kayit_Bilg,Egitim_Fotograflari")] Egitimler egitimler)
        {
            if (ModelState.IsValid)
            {
                db.Egitimler.Add(egitimler);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(egitimler);
        }

        // GET: Admin/Egitimlers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Egitimler egitimler = db.Egitimler.Find(id);
            if (egitimler == null)
            {
                return HttpNotFound();
            }
            return View(egitimler);
        }

        // POST: Admin/Egitimlers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Egitim_Adi,Egitim_Tanitimi,Egitim_Amaci,Egitim_Faydasi,Sertifika,Yer,Zaman,Ucret,Kontenjan,Tatbikat_Bilg,Seminer_Bilg,Kayit_Bilg,Egitim_Fotograflari")] Egitimler egitimler)
        {
            if (ModelState.IsValid)
            {
                db.Entry(egitimler).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(egitimler);
        }

        // GET: Admin/Egitimlers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Egitimler egitimler = db.Egitimler.Find(id);
            if (egitimler == null)
            {
                return HttpNotFound();
            }
            return View(egitimler);
        }

        // POST: Admin/Egitimlers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Egitimler egitimler = db.Egitimler.Find(id);
            db.Egitimler.Remove(egitimler);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
