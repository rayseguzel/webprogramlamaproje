﻿using saumfsertifika.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace saumfsertifika.Controllers
{
    public class HomeController : Controller

    {
        sertifikaegitimEntities db = new sertifikaegitimEntities();

        public ActionResult Index()
        {
            

          var duyuru= db.Duyurular.OrderByDescending(x => x.Duyuru_Tarih).Take(3).ToList();
      


            return View(duyuru);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Egitmenler()
        {
            var egitmen = db.Egitmenler.ToList();

            return View(egitmen);
        }




        public ActionResult Gallery()
        {
            ViewBag.Message = "Your gallery page.";

            return View();
        }

        public ActionResult YanginEgiticiEgitimi()
        {
            ViewBag.Message = "Your Yangın Eğitici Eğitimi page.";

            return View();
        }

        public ActionResult YanginTahliyeAramaKurtarma()
        {
            ViewBag.Message = "Your Yangın Tahliye, Arama/Kurtarma Eğitici Eğitimi page.";

            return View();
        }

        public ActionResult YuksekGerilimCokYuksekGerilim()
        {
            ViewBag.Message = "Your Yüksek Gerilim ve Çok Yüksek Gerilim Tekniği Eğitimi page.";

            return View();
        }


        public ActionResult Elektrik()
        {
            ViewBag.Message = "Your Elektrik Üretim Tesisleri, Elektrik Sistem Tasarımı ve Ekipman Seçimi Eğitimi page.";

            return View();
        }


        public ActionResult RuzgarveGunesElektrikUretim()
        {
            ViewBag.Message = "Your Rüzgar ve Güneş Elektrik Üretim Santralleri Tasarımı Eğitimi page.";

            return View();
        }


        public ActionResult AfetYonetimiEgitimi()
        {
            ViewBag.Message = "Your Afet Yönetimi Eğitimi page.";

            return View();
        }


        public ActionResult PLC()
        {
            ViewBag.Message = "Your PLC Eğitimi page.";

            return View();
        }


        public ActionResult SesveGurultu()
        {
            ViewBag.Message = "Your Temel Ses ve Gürültü Ölçümleri Eğitimi page.";

            return View();
        }


        public ActionResult TemelTitresim()
        {
            ViewBag.Message = "Your Temel Titreşim Ölçümleri ve Analizleri Eğitimi page.";

            return View();
        }


        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }

       

    }

    public class AnaSayfaDTO
    {
        public List<Slider> slider { get; set; }
        public List<Duyurular> duyuru { get; set; }
    }


}