﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using saumfsertifika.Models;

namespace saumfsertifika.Controllers
{
    public class Egitim_KayitliController : Controller
    {
        private sertifikaegitimEntities db = new sertifikaegitimEntities();

        // GET: Egitim_Kayitli
        public ActionResult Index()
        {
            var egitim_Kayitli = db.Egitim_Kayitli.Include(e => e.Egitimler).Include(e => e.Kayitli);
            return View(egitim_Kayitli.ToList());
        }

        // GET: Egitim_Kayitli/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Egitim_Kayitli egitim_Kayitli = db.Egitim_Kayitli.Find(id);
            if (egitim_Kayitli == null)
            {
                return HttpNotFound();
            }
            return View(egitim_Kayitli);
        }

        // GET: Egitim_Kayitli/Create
        public ActionResult Create()
        {
            ViewBag.FKEgitim_id = new SelectList(db.Egitimler, "ID", "Egitim_Adi");
            ViewBag.FKKayitli_id = new SelectList(db.Kayitli, "ID", "Ad");
            return View();
        }

        // POST: Egitim_Kayitli/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FKEgitim_id,FKKayitli_id")] Egitim_Kayitli egitim_Kayitli)
        {
            if (ModelState.IsValid)
            {
                db.Egitim_Kayitli.Add(egitim_Kayitli);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FKEgitim_id = new SelectList(db.Egitimler, "ID", "Egitim_Adi", egitim_Kayitli.FKEgitim_id);
            ViewBag.FKKayitli_id = new SelectList(db.Kayitli, "ID", "Ad", egitim_Kayitli.FKKayitli_id);
            return View(egitim_Kayitli);
        }

        // GET: Egitim_Kayitli/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Egitim_Kayitli egitim_Kayitli = db.Egitim_Kayitli.Find(id);
            if (egitim_Kayitli == null)
            {
                return HttpNotFound();
            }
            ViewBag.FKEgitim_id = new SelectList(db.Egitimler, "ID", "Egitim_Adi", egitim_Kayitli.FKEgitim_id);
            ViewBag.FKKayitli_id = new SelectList(db.Kayitli, "ID", "Ad", egitim_Kayitli.FKKayitli_id);
            return View(egitim_Kayitli);
        }

        // POST: Egitim_Kayitli/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FKEgitim_id,FKKayitli_id")] Egitim_Kayitli egitim_Kayitli)
        {
            if (ModelState.IsValid)
            {
                db.Entry(egitim_Kayitli).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FKEgitim_id = new SelectList(db.Egitimler, "ID", "Egitim_Adi", egitim_Kayitli.FKEgitim_id);
            ViewBag.FKKayitli_id = new SelectList(db.Kayitli, "ID", "Ad", egitim_Kayitli.FKKayitli_id);
            return View(egitim_Kayitli);
        }

        // GET: Egitim_Kayitli/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Egitim_Kayitli egitim_Kayitli = db.Egitim_Kayitli.Find(id);
            if (egitim_Kayitli == null)
            {
                return HttpNotFound();
            }
            return View(egitim_Kayitli);
        }

        // POST: Egitim_Kayitli/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Egitim_Kayitli egitim_Kayitli = db.Egitim_Kayitli.Find(id);
            db.Egitim_Kayitli.Remove(egitim_Kayitli);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
